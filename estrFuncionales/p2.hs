-- parctica 2
-- ==========
-- Ejemplos

add :: (Int,Int) -> Int
add (x,y) = x + y

deceroa :: Int -> [Int]
deceroa n = [0..n]

-- ========================================================
-- Ejercicio 1
-- ========================================================
f a = 4

test :: (Num a, Eq a) => (a -> a) -> a -> Bool
test f x = f x == x + 2

esMenor :: Ord a =>  a -> a -> Bool
esMenor a b = a <= b

eq :: Eq a => a -> a -> Bool
eq a b = a == b

showVal:: Show a => a -> String
showVal x = "valor:" ++ show x

-- ========================================================
-- Ejercicio 2
-- ========================================================

-- (+5):: (Num a) => a -> a
-- suma 5 a un numero

-- (0<):: (Num a,Ord a) => a -> Bool
-- cmp si es menor a cero

-- ('a':):: String -> String
-- concatena 'a' a un str

-- (++"\n"):: String ->  String
-- concatena salto de linea a un str

-- filter (==7):: (Eq a, Num a) => [a] -> [a]
-- filtra los elem que son == a 7 de un arreglo

-- map (++[1]):: Num a => [[a]] -> [[a]]
-- a cada item lista se le añade 1

-- ========================================================
 -- Ejercicio 3
-- ========================================================

five ani = 5

fa:: (Int -> Int) -> Int
fa g  = g 5

fb:: Int -> (Int -> Int)
fb a b = a + b

fc five a = 10

fd :: Int -> Bool
fd a = True

fe :: Bool -> (Bool -> Bool)
fe x y = not x

ff :: (Int, Char) -> Bool
ff (a,b) = True

fg :: (Int, Int) -> Int
fg (n1,n2) = n1 + n2 -- cual es la diferencia con el b

fh :: Int -> (Int,Int)
fh  n = (n + 1, n - 1)

fi :: a -> Bool
fi a = True

fj :: a -> a
fj a = a

-- ========================================================
 -- Ejercicio 4
-- ========================================================
{-
a) False
b) Error sintactico
c) False
d) Error de tipos
e) 0
f) Error de tipo
g) True

-}

-- ========================================================
 -- Ejercicio 5
-- ========================================================??


greater (x,y) | x > y = True
              | otherwise = False

greater' (x,y) = x > y


-- ========================================================
 -- Ejercicio 6
-- ========================================================


-- ========================================================
 -- Ejercicio 7
-- ========================================================


-- ========================================================
 -- Ejercicio 8
-- ========================================================


-- ========================================================
 -- Ejercicio 9
-- ========================================================







-- ========================================================
-- Ejercicio 10
-- ========================================================
-- a
-- Tiene sentido si xs es unalista de tipo [[t]] o []
-- No es valido
-- [[],xs]
--
-- b
-- Tiene sentido si xs es unalista de tipo [[t]] o []
-- No es valido
-- [[],xs]
--
-- c
-- Tiene sentido si xs es unalista de tipo [[t]] o []
-- es valido
-- [[],xs]
--
-- d
-- Tiene sentido si xs es unalista de tipo [[t]] o []
-- no es valido
-- [[],[t]]
--
-- e
-- Tiene sentido si xs es una lista de tipo [t]
-- es valido
-- -
--
-- f
-- Tiene sentido si xs es una lista de tipo [t]
-- No es valido
-- [[],xs]
--
-- g
-- Tiene sentido si xs es una lista de tip [t] o []
-- No es valido
-- xs
--
-- h
-- Tiene sentido si xs es una lista de tip [t] o []
-- Es valido
-- _
--
-- i
-- Tiene sentido si xs es una lista de tip [t] o []
-- Es valido
-- _
--
-- j
-- Tiene sentido si xs es una lista de tip [t] o []
-- Es valido
-- _

-- ========================================================
-- 11
-- ========================================================
-- [Float] -> Float
-- [Float] -> [Float]

-- ========================================================
-- 12
-- ========================================================
type NumBin = [Bool]

sumaBianria :: NumBin -> NumBin -> NumBin
sumaBianria a b = a ++ b??
